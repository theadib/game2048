# Project Game2048

This is a clone of the popular 2048 game.

The project is used to learn programming and Python.
The Project is realized using Python programming language and the PyGame library.

# Operation

The game is operated using the cursor keys.

some other keys:
- ESC quit
- N new game

When there is no possible move, then the game stops.
You have to restart from Zero.

