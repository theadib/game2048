#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 21 23:05:41 2021

@author: lindataraben

access to the board is game.board[row][collum]=2, row, collum: 0 ... 3
direction is "u" for up, "d" for down, "l" for left, "r" for right
"""

import random

class Game2048:
    def __init__(self):
        self.board = []
        for row in range(4):
            self.board.append([0] *4)
        # for testing only
        # self.board[0][0] = 2
        # self.board[2][2] = 128
        self.score = 0
    
    def rotate(self, direction):
        ''' rotate the game to simplify algorithm '''
        if direction in ["u", "d"]:
            # switch row column elements, need orthogonal board, uses swap
            for row in range(len(self.board)-1):
                for column in range(row+1, len(self.board)):
                    self.board[column][row], self.board[row][column] = self.board[row][column], self.board[column][row]
        
        if direction in ["r", "d"]:
            for row in range(len(self.board)):
                self.board[row].reverse()

    def rotate_corected(self, direction):
        ''' rotate the game to simplify algorithm '''
        if direction in ["r", "d"]:
            for row in range(len(self.board)):
                self.board[row].reverse()

        if direction in ["u", "d"]:
            # switch row column elements, need orthogonal board, uses swap
            # print(f'direction {direction} swap: ', self.board)
            for row in range(len(self.board)-1):
                # print(f"row: {row}")
                for column in range(row+1, len(self.board)):
                    # print(f"swap {row} : {column}")
                    self.board[column][row], self.board[row][column] = self.board[row][column], self.board[column][row]
            # print(self.board)

    def play(self, direction):
        print(direction)
        self.rotate(direction)
        # assuming left direction
        for row in range(len(self.board)):
            columncount = len(self.board[row])
            # compress: remove zeroes
            while 0 in self.board[row]:
                self.board[row].remove(0)
            # merge if possible
            if 2 <= len(self.board[row]):
                # there are at least 2(two) elements
                for colum in range(len(self.board[row])-1):
                    # also test list length here
                    if colum < len(self.board[row])-1 and self.board[row][colum] == self.board[row][colum+1]:
                        # merge two elements that are same value
                        self.board[row][colum] = 2*self.board[row][colum]
                        # todo: increase the score  !!
                        # removed the merged element
                        self.board[row].pop(colum+1)
            # fill zeroes again to the back of the list
            while columncount > len(self.board[row]):
                self.board[row].append(0)
        self.rotate_corected(direction)

    def new_game(self):
        print('new game')
        self.board = []
        for row in range(4):
            self.board.append([0] *4)
        self.score = 0
    
    def add_random_element(self):
        ''' pick a random coordinade from free fileds and put a 2 '''
        # create freelist
        freelist = []
        for row in range(len(self.board)):
            for column in range(len(self.board[row])):
                if self.board[row][column] == 0:
                    freelist.append((row,column))
        # pick random element if any
        if len(freelist):
            index = random.randrange(len(freelist))
            row, column = freelist[index]
            self.board[row][column] = 2
            return freelist[index]
        # no item available
        return None

    def check_won(self):
        ''' check winning condition 2048 in any cell '''
        for row in self.board:
            if 2048 in row:
                return True
        return False
        
    def check_lost(self):
        ''' check loosing condition none cell is empty '''
        for row in self.board:
            if 0 in row:
                return False
        return True

    def tostr(self):
        # for debug only
        print ("hello game2048")
        print(self.board)
        


        
if __name__ == "__main__":       
     game=Game2048()
     game.tostr()
     game.board[1][1]=2
     game.tostr()
     print(game.check_won())
     game.board[1][1]=2048
     print(game.check_won())
