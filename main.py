#!/usr/bin/env python3
'''
    pygame based gui for 2048 game
    inspired by https://github.com/danlls/snake-pygame snake implementation

    font and sound taken from https://github.com/danlls/snake-pygame
    colour codes taken from https://scrambledeggsontoast.github.io/2014/05/09/writing-2048-elm/

    window is of size (600:width, 800:height)
    point (0,0) is in upper left corner

'''
import pygame
import sys

import game2048

FRAMERATE = 20

SCREEN_WIDTH = 600
SCREEN_HEIGHT = 800


DEFAULT_FONT = 'freesansbold.ttf'

BACKGROUND_COLOUR = (57, 52,49)
SHADDOW_COLOUR = (200, 200, 200, 150)
TEXT_COLOUR = (42,39,36)

# Color RGB values
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
BLUE = (95, 135, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
DARK_GREEN = (0, 200, 0)
DARK_BLUE = (65, 105, 225)
DARK_RED = (200, 0, 0)
GREY = (211, 211, 211)

tile_colours = {
        0:(211, 211, 211), 
        2:(238, 228, 218), 
        4:(237, 224, 200),
        8:(242, 177, 121),
        16:(245, 149, 99),
        32:(246, 124, 95),
        64:(246, 94, 59),
        128:(237, 207, 114),
        256:(237, 204, 97),
        512:(237, 200, 80),
        1024:(237, 197, 63),
        2048: (237, 194, 46),
    }


            


class App:

    def __init__(self, width=SCREEN_WIDTH, height=SCREEN_HEIGHT):  
        pygame.mixer.init()
        pygame.init()
        pygame.font.init()
        self.screen_width = width
        self.screen_height = height
        self.screen = pygame.display.set_mode([self.screen_width, self.screen_height])
        pygame.display.set_caption('2048')
        self.font = pygame.font.Font(DEFAULT_FONT, 60)
        self.big_font = pygame.font.Font(DEFAULT_FONT, 120)
        self.eat_sound = pygame.mixer.Sound('8biteat.wav')
        self.you_won_sound = pygame.mixer.Sound('8biteat.wav')
        self.you_lost_sound = pygame.mixer.Sound('8biteat.wav')

        self.game = game2048.Game2048()
        self.is_running = True

    def quit(self):
        pygame.quit()
        self.is_running = False

    def draw_board(self):
        ''' actually draws the board '''
        # print('draw:', self.game.board)
        rowcount = len(self.game.board)
        margin = self.screen_width/100
        messageheight = self.screen_height-self.screen_width

        self.screen.fill(BACKGROUND_COLOUR)
        # draw tiles
        rowheight = (self.screen_height-messageheight)/rowcount
        height = rowheight-margin
        for row in range(rowcount):
            columncount = len(self.game.board[row])
            y0 = messageheight + row*rowheight + margin/2

            # draw column within row
            columnwidth = self.screen_width/columncount
            width = columnwidth-margin
            for column in range(columncount):
                x0 = column*columnwidth + margin/2
                # print(f'x:{column}, y:{row}, value:{self.game.board[row][column]}')
                #draw brick
                pygame.draw.rect(self.screen, tile_colours.get(self.game.board[row][column], DARK_GREEN), (x0,y0, width,height))
                text = str(self.game.board[row][column])
                tile_text = self.font.render(text, True, TEXT_COLOUR)
                # centre the text
                textwith, textheight = pygame.font.Font.size(self.font, text)
                self.screen.blit(tile_text, (x0+(width-textwith)/2, y0+(height-textheight)/2))

        # draw message
        score_text = self.font.render("Score: " + str(self.game.score), True, RED)
        self.screen.blit(score_text, (0,0))

    def you_won(self):
        self.draw_board()
        pygame.draw.rect(self.screen, SHADDOW_COLOUR, (0,0, self.screen_width,self.screen_height))
        winner_text = self.big_font.render(" You won!", True, RED)
        self.screen.blit(winner_text, (0,self.screen_height/4))
        winner_text = self.font.render(" press ENTER", True, RED)
        self.screen.blit(winner_text, (0,self.screen_height/2))
        pygame.display.update()
        self.you_won_sound.play()
        # wait for any key
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.quit()
                if event.type == pygame.KEYDOWN:
                    if event.key in [pygame.K_KP_ENTER, pygame.K_RETURN]:
                        self.game.new_game()
                        return
        
    def you_lost(self):
        self.screen.fill(SHADDOW_COLOUR)
        winner_text = self.big_font.render("You lost!", True, RED)
        self.screen.blit(winner_text, (0,self.screen_height/4))
        winner_text = self.font.render(" press ENTER", True, RED)
        self.screen.blit(winner_text, (0,self.screen_height/2))
        pygame.display.update()
        self.you_won_sound.play()
        # wait for any key
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.quit()
                if event.type == pygame.KEYDOWN and event.key == pygame.K_KP_ENTER:
                    self.game.new_game()
                    return

    def run(self):
        # draw initial state
        self.draw_board()
        pygame.display.update()
        
        # wait for keys and update 
        while self.is_running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.quit()
                if event.type == pygame.KEYDOWN:
                    # Only allow left or right movement when snake is moving 
                    # in vertical direction
                    if event.key == pygame.K_ESCAPE:
                        self.quit()
                    elif event.key == pygame.K_n:
                        self.game.new_game()
                    elif event.key == pygame.K_LEFT or event.key == pygame.K_KP4:
                        self.game.play('l')
                    elif event.key == pygame.K_RIGHT or event.key == pygame.K_KP6:
                        self.game.play('r')
                    elif event.key == pygame.K_UP or event.key == pygame.K_KP8:
                        self.game.play('u')
                        self.game.score += 1
                    elif event.key == pygame.K_DOWN or event.key == pygame.K_KP2:
                        self.game.play('d')

                    # for testing only, don't cheat
                    elif event.key == pygame.K_m:
                        self.game.board[0][0] = 2 * self.game.board[0][0]
                    elif event.key == pygame.K_o:
                        self.you_won()
                    elif event.key == pygame.K_l:
                        self.you_lost()
                    self.eat_sound.play()

                    # after action just add a new element 
                    self.game.add_random_element()

                    # check end conditions
                    if self.game.check_won():
                        self.you_won()
                    elif self.game.check_lost():
                        self.you_lost()

                    # draw board
                    self.draw_board()
                    pygame.display.update()


if __name__ == "__main__":
    app = App()
    app.run()
